<?php
/**
 * The default template for displaying content
 *
 * @author      Nanozum
 * @link        http://nanozum.co
 * @copyright   Copyright (c) 2015 Nanozum
 * @license     GPL v2
 */
$format = get_post_format();
$placeholder_image = get_template_directory_uri(). '/assets/images/layzyload-grid.jpg';
//share
$share = get_theme_mod('zum_post_meta_share', false);
?>

<article <?php post_class('post-item post-grid  clearfix'); ?>>
    <div class="article-tran">
            <?php if(!get_theme_mod('sp_post_thumb')) : ?>
                <?php if(has_post_format('gallery')) : ?>
                    <?php $images = get_post_meta( $post->ID, '_format_gallery_images', true ); ?>
                    <?php if($images) : ?>
                        <div class="post-image single-image">
                            <ul class="owl-single">
                                <?php foreach($images as $image) : ?>
                                    <?php $the_image = wp_get_attachment_image_src( $image, 'zum-single-post' ); ?>
                                    <?php $the_caption = get_post_field('post_excerpt', $image); ?>
                                    <li><img src="<?php echo esc_url($the_image[0]); ?>" <?php if($the_caption) : ?>title="<?php echo esc_attr($the_caption); ?>"<?php endif; ?> /></li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                    <?php endif; ?>
                <?php else : ?>
                    <?php if(has_post_thumbnail()) : ?>
                    <?php $thumbnail_src = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), "zum-blog-grid" ); ?>
                    <div class="image-item">
                        <a href=" <?php echo get_permalink() ?>">
                            <img  class="lazy " src="<?php echo esc_url($placeholder_image);?>"  data-original="<?php echo esc_attr($thumbnail_src[0]);?>" data-lazy="<?php echo esc_attr($thumbnail_src[0]);?>" alt="post-image"/>
                        </a>
                    </div>
                <?php endif;?>
                <div class="article-content">
                    <span class="post-cat"><?php echo zum_category(' '); ?></span>
                    <div class="entry-header clearfix">
                        <header class="entry-header-title">
                            <?php
                            the_title( sprintf( '<h3 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h3>' );
                            ?>
                        </header>
                    </div>
                    <div class="article-meta clearfix">
                        <?php
                        get_template_part('templates/share-grid');
                        ?>
                    </div>
                </div>
            <?php endif; ?>
        <?php else :
            $placeholder_image = get_template_directory_uri(). '/assets/images/placeholder-trans.png';
            ?>
            <div class="post-image  placeholder-trans">
            </div>
            <div class="article-content no-thumb">
                <span class="post-cat"><?php echo zum_category(' '); ?></span>
                <div class="entry-header clearfix">
                    <header class="entry-header-title">
                        <?php
                        the_title( sprintf( '<h3 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h3>' );
                        ?>
                    </header>
                </div>
                <div class="article-meta clearfix">
                    <?php zum_entry_meta(); ?>
                </div>
                <div class="entry-content">
                    <?php
                    if ( has_excerpt() || is_search() ){
                        zum_excerpt();
                    }
                    else{
                        echo zum_content(25);
                    }
                    wp_link_pages( array(
                        'before'      => '<div class="page-links"><span class="page-links-title">' . esc_html__( 'Pages:', 'zum' ) . '</span>',
                        'after'       => '</div>',
                        'link_before' => '<span class="page-numbers">',
                        'link_after'  => '</span>',
                        'pagelink'    => '<span class="screen-reader-text">' . esc_html__( 'Page', 'zum' ) . ' </span>%',
                        'separator'   => '<span class="screen-reader-text">, </span>',
                    ) );
                    ?>
                </div>
                <a class="readmore" href="<?php echo get_permalink() ?>"><?php esc_html_e('Read more','zum'); ?></a>

            </div>
        <?php endif; ?>
    </div>

</article><!-- #post-## -->
