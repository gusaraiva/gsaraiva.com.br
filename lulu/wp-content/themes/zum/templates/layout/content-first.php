<?php
/**
 * The default template for displaying content
 *
 * @author      NanoAgency
 * @link        http://nanoagency.co
 * @copyright   Copyright (c) 2015 NanoAgency
 * @license     GPL v2
 */

$format = get_post_format();
$format = get_post_format();
$add_class='';

//number word content
$number_word = get_theme_mod('zum_number_content_post','45');
$placeholder_image = get_template_directory_uri(). '/assets/images/layzyload-list.jpg';
$share = get_theme_mod('zum_post_meta_share', false);
?>
<article  <?php post_class('post-item post-list clearfix'); ?>>
    <div class="article-image">
        <?php if(has_post_format('gallery')) : ?>
            <?php $images = get_post_meta( get_the_ID(), '_format_gallery_images', true );?>
            <?php if($images) : ?>
                <div class="post-image single-image">
                    <ul class="owl-single">
                        <?php foreach($images as $image) : ?>
                            <?php $the_image = wp_get_attachment_image_src( $image, 'zum-first-post' ); ?>
                            <?php $the_caption = get_post_field('post_excerpt', $image); ?>
                            <li><img src="<?php echo esc_url($the_image[0]); ?>" <?php if($the_caption) : ?>title="<?php echo esc_attr($the_caption); ?>"<?php endif; ?> /></li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            <?php endif; ?>

        <?php elseif(has_post_format('video')) : ?>
            <div class="post-video">
                <a  href="<?php echo get_post_format_link( $format ); ?>" class="post-format"><i class="ti-control-play"></i></a>
            </div>
        <?php elseif(has_post_format('audio')) : ?>
            <div class="post-audio">
                <a href="<?php echo get_permalink() ?>"><?php the_post_thumbnail('zum-blog-list'); ?></a>
                <a  href="<?php echo get_post_format_link( $format ); ?>" class="post-format"><i class="ti-headphone"></i></a>
            </div>
        <?php elseif(has_post_format('quote')) :?>
            <div class="post-quote <?php echo esc_attr($add_class);?>">
                <?php $sp_quote = get_post_meta( get_the_ID(), '_format_quote_source_name', true ); ?>
                <a href="<?php echo get_permalink() ?>"><?php the_post_thumbnail('zum-blog-list'); ?></a>
                <a  href="<?php echo get_post_format_link( $format ); ?>" class="post-format"><i class="ti-quote-left"></i></a>
            </div>
        <?php else:
            if(has_post_thumbnail()) { ?>
                <?php $thumbnail_src = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), "zum-first-post" ); ?>
                <div class="post-image">
                    <a href="<?php echo get_permalink();?>">
                        <img  class="lazy" src="<?php echo esc_url($placeholder_image);?>"  data-original="<?php echo esc_attr($thumbnail_src[0]);?>" data-lazy="<?php echo esc_attr($thumbnail_src[0]);?>" alt="post-image"/>
                    </a>
                </div>
            <?php } else {
                $add_class='full-width';
            }
        endif; ?>
    </div>
    <div class="article-content <?php echo esc_attr($add_class);?>">
        <div class="entry-header clearfix">
            <span class="post-cat"><?php echo zum_category(' '); ?></span>
            <header class="entry-header-title">
                <?php
                the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' );
                ?>
            </header>
            <div class="entry-avatar clearfix">
                <span class="author-by"><?php echo esc_html__('By','zum')?></span>
            <span class="author-title">
                <a class="author-link" href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>" rel="author">
                    <?php echo esc_attr(get_the_author()); ?>
                </a>
            </span>
            <span class="post-date">
                <?php echo get_the_date('M d, Y'); ?>
            </span>
            </div>
        </div>
        <div class="entry-content">
            <?php
            if ( has_excerpt() || is_search() ){
                zum_excerpt();
            }
            else{
                echo zum_content($number_word);
            }
            wp_link_pages( array(
                'before'      => '<div class="page-links"><span class="page-links-title">' . esc_html__( 'Pages:', 'zum' ) . '</span>',
                'after'       => '</div>',
                'link_before' => '<span class="page-numbers">',
                'link_after'  => '</span>',
                'pagelink'    => '<span class="screen-reader-text">' . esc_html__( 'Page', 'zum' ) . ' </span>%',
                'separator'   => '<span class="screen-reader-text">, </span>',
            ) );
            ?>

        </div>
        <div class="entry-footer clearfix">

            <a class="readmore" href="<?php echo get_permalink() ?>"><?php esc_html_e('read more','zum'); ?></a>
            <?php
            get_template_part('templates/share');
            ?>
        </div>
    </div>
</article><!-- #post-## -->
