
<?php if(get_theme_mod('zum_enable_footer', '1')) { ?>
    <footer id="na-footer" class="na-footer  footer-2">
        <?php
        if(is_active_sidebar( 'footer-column1' ) || is_active_sidebar( 'footer-column2' ) || is_active_sidebar( 'footer-column3' ) || is_active_sidebar( 'footer2-column4') || is_active_sidebar( 'footer-top')){ ?>
            <!--    Footer center-->
            <div class="clearfix">
                <div class="footer-top">
                    <?php dynamic_sidebar('footer-top'); ?>
                </div>
                <div class="container">
                    <div class="container-inner">
                        <div class="row">
                            <div class="col-md-4 col-sm-6 col-xs-6">
                                <?php dynamic_sidebar('footer-column1'); ?>
                            </div>
                            <div class="col-md-2 col-sm-6 col-xs-6">
                                <?php dynamic_sidebar('footer-column2'); ?>
                            </div>
                            <div class="col-md-2 col-sm-3 col-xs-4">
                                <?php dynamic_sidebar('footer-column3'); ?>
                            </div>
                            <div class="col-md-4 col-sm-9 col-xs-8">
                                <?php dynamic_sidebar('footer2-column4'); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php }?>
        <!--    Footer bottom-->
        <div class="footer-bottom clearfix">
            <div class="container">
                <div class="container-inner">
                    <div class="row">

                        <div class="col-md-12 col-sm-12">
                            <?php if(get_theme_mod('zum_enable_copyright', '1')) { ?>
                                <div class="copy-right">
                                    <?php if(get_theme_mod('zum_copyright_text')) {?>
                                        <span><?php echo get_theme_mod('zum_copyright_text');?></span>
                                    <?php } else {
                                        echo '<i class="fa fa-copyright" aria-hidden="true"></i> ' .date(" Y "). '<a href="'.esc_url('http://zum.nanoagency.co').'">'.esc_html('ZUM. ') . '</a>'.esc_html('All Rights Reserved. Design by').'<a href="'.esc_url('http://nanoagency.co').'">'. esc_html(' iDoodle ').'</a>';
                                    } ?>
                                </div><!-- .site-info -->
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer><!-- .site-footer -->
    <div id="scrollup" class="scrollup"><i class="fa fa-angle-up"></i></div>
<?php } ?>