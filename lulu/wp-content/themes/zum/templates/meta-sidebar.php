
<?php
$comments_facebook = get_theme_mod('zum_comments_single_facebook',false);
$views 		= get_theme_mod('zum_cat_content_view',true);
$date 		= get_theme_mod('zum_post_meta_date',true);
?>
<div class="entry-footer-social clearfix">
    <div class="entry-footer-left">
        <?php
        if ( in_array( get_post_type(), array( 'post', 'attachment' ) ) && $date ) {
            $time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';

            if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
                $time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time><time class="updated" datetime="%3$s">%4$s</time>';
            }

            $time_string = sprintf( $time_string,
                esc_attr( get_the_date( 'c' ) ),
                get_the_date(),
                esc_attr( get_the_modified_date( 'c' ) ),
                get_the_modified_date()
            );

            printf( '<span class="posted-on"><a href="%2$s" rel="bookmark">%3$s</a></span>',
                _x( '', 'Used before publish date.', 'zum' ),
                esc_url( get_permalink() ),
                $time_string
            );
        }
        ?>
        <div class="comment-text">
            <a class = "comment-text" href="<?php echo esc_url(comments_link());?>">
                <?php if($comments_facebook){?>
                    <span class="fb-comments-count" data-href="<?php echo esc_url(get_permalink()) ?>"></span><span class="txt"><?php echo esc_html__('Comments','zum')?></span>
                <?php } else{?>
                    <span class="text-comment"><?php comments_number( '0 Comments', '1 Comment', '% Comments' ); ?></span>
                <?php }?>
            </a>
        </div>
    </div>
</div>

