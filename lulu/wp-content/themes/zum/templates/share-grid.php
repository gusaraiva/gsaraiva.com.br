
<?php
$comments_facebook = get_theme_mod('zum_comments_single_facebook',false);
$views 		= get_theme_mod('zum_cat_content_view',true);
$date 		= get_theme_mod('zum_post_meta_date',true);
$share 		= get_theme_mod('zum_post_meta_share',false);
?>
<div class="entry-footer-social clearfix">
    <div class="entry-footer-left">
        <?php
        if ( in_array( get_post_type(), array( 'post', 'attachment' ) ) && $date ) {
            $time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';

            if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
                $time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time><time class="updated" datetime="%3$s">%4$s</time>';
            }

            $time_string = sprintf( $time_string,
                esc_attr( get_the_date( 'c' ) ),
                get_the_date(),
                esc_attr( get_the_modified_date( 'c' ) ),
                get_the_modified_date()
            );

            printf( '<span class="posted-on"><span class="screen-reader-text">%1$s </span><a href="%2$s" rel="bookmark">%3$s</a></span>',
                _x( '', 'Used before publish date.', 'zum' ),
                esc_url( get_permalink() ),
                $time_string
            );
        }
        ?>
        <div class="comment-text">
            <a class = "comment-text" href="<?php echo esc_url(comments_link());?>">
                <?php if($comments_facebook){?>
                    <span class="fb-comments-count" data-href="<?php echo esc_url(get_permalink()) ?>"></span><span class="txt"><?php echo esc_html__('Comments','zum')?></span>
                <?php } else{?>
                    <span class="text-comment"><?php comments_number( '0 Comments', '1 Comment', '% Comments' ); ?></span>
                <?php }?>
            </a>
        </div>
        <?php if ($share) {?>
        <div class="entry-footer-right">
            <span><?php echo esc_html__('Share','zum')?></span>
            <div class="social share-links clearfix">
                <?php
                $share_facebook     = get_theme_mod('zum_share_facebook',true);
                $share_twitter      = get_theme_mod('zum_share_twitter',true);
                $share_google       = get_theme_mod('zum_share_google',true);
                $share_linkedin     = get_theme_mod('zum_share_linkedin',false);
                $share_pinterest    = get_theme_mod('zum_share_pinterest',false);
                ?>
                <div class="count-share">
                    <ul class="social-icons list-unstyled list-inline">
                        <?php if ($share_facebook):?>
                            <li class="social-item facebook">
                                <a href="http://www.facebook.com/sharer.php?u=<?php the_permalink(); ?>" title="<?php echo esc_html__('facebook', 'zum'); ?>" class="post_share_facebook facebook" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=220,width=600');return false;">
                                    <i class="fa fa-facebook"></i>
                                </a>
                            </li>
                        <?php endif; ?>
                        <?php if ($share_twitter):?>
                            <li class="social-item twitter">
                                <a href="https://twitter.com/share?url=<?php the_permalink(); ?>" title="<?php echo esc_html__('twitter', 'zum'); ?>" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=260,width=600');return false;" class="product_share_twitter twitter">
                                    <i class="fa fa-twitter"></i>
                                </a>
                            </li>
                        <?php endif; ?>
                        <?php if ($share_google):?>
                            <li class="social-item google">
                                <a href="https://plus.google.com/share?url=<?php the_permalink(); ?>" class="googleplus" title="<?php echo esc_html__('google +', 'zum'); ?>" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;">
                                    <i class="fa fa-google-plus"></i>
                                </a>
                            </li>
                        <?php endif; ?>
                        <?php if ($share_linkedin):?>
                            <li class="social-item linkedin">
                                <a href="https://www.linkedin.com/shareArticle?mini=true&url=<?php the_permalink();?>&title=<?php echo esc_html__('pinterest', 'zum'); ?>&summary=<?php echo get_the_title(); ?>&source=<?php echo get_the_title(); ?>">
                                    <i class="fa fa-linkedin"></i>
                                </a>
                            </li>
                        <?php endif; ?>
                        <?php if ($share_pinterest):?>
                            <li class="social-item pinterest">
                                <a href="http://pinterest.com/pin/create/button/?url=<?php the_permalink(); ?>&media=<?php if(function_exists('the_post_thumbnail')) echo wp_get_attachment_url(get_post_thumbnail_id()); ?>&description=<?php echo get_the_title(); ?>" title="<?php echo esc_html__('pinterest', 'zum'); ?>" class="pinterest">
                                    <i class="fa fa-pinterest"></i>
                                </a>
                            </li>
                        <?php endif; ?>
                    </ul>
                </div>
            </div>
        </div>
        <?php }?>
    </div>

</div>

