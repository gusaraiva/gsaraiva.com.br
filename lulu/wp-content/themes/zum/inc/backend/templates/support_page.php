<?php
/**
 * Support Theme Page
 *
 * @author      NanoAgency
 * @link        http://nanoagency.co
 * @copyright   Copyright (c) 2015 NanoAgency
 * @license     GPL v2
 */
?>
<div class="nano-support-plugins">
    <div class="theme-browser rendered">
        <p class="need-help"><?php echo esc_html__('If you meet difficult when install or use theme .Please create ', 'zum'); ?> <a href="<?php echo esc_url('http://help.nanoagency.co/');?>" target="_blank"><?php echo esc_html__('Ticket', 'zum'); ?><span class="dashicons dashicons-external"></span></a><?php echo esc_html__(' send to us','zum');?></p>
        <p class="need-help"><?php echo esc_html__('Need help getting started? Check out the Theme\'s Documentation', 'zum'); ?> <a href="<?php echo esc_url('http://guide.nanoagency.co/zum');?>" target="_blank" <span class="dashicons dashicons-format-aside"></span></a></p>
        <h4><?php echo esc_html__('Keep in Touch','zum');?></h4>
        <ul class="support-social">
            <li><a target="_blank" href="<?php echo esc_url('mailto:nmc2010@gmail.com')?>"><span class="dashicons dashicons-email-alt"></span><?php echo esc_html__('Newsletter','zum');?></a></li>
            <li><a target="_blank" href="<?php echo esc_url('https://twitter.com/nmc20101')?>" ><span class="dashicons dashicons-twitter"></span><?php echo esc_html__('Twitter','zum');?></a></li>
            <li><a target="_blank" href="<?php echo esc_url('https://www.facebook.com/themenano')?>"><span class="dashicons dashicons-facebook"></span><?php echo esc_html__('Facebook','zum');?></a></li>
        </ul>
    </div>
</div>
