<?php
/**
 * @package     NA Core
 * @version     0.1
 * @author      Nanozum
 * @link        http://nanozum.co
 * @copyright   Copyright (c) 2015 Nanozum
 * @license     GPL v2
 */
if (!class_exists('zum_Customize')) {
    class zum_Customize
    {
        public $customizers = array();

        public $panels = array();

        public function init()
        {
            $this->customizer();
            add_action('customize_controls_enqueue_scripts', array($this, 'zum_customizer_script'));
            add_action('customize_register', array($this, 'zum_register_theme_customizer'));
            add_action('customize_register', array($this, 'remove_default_customize_section'), 20);
        }

        public static function &getInstance()
        {
            static $instance;
            if (!isset($instance)) {
                $instance = new zum_Customize();
            }
            return $instance;
        }

        protected function customizer()
        {
            $this->panels = array(

                'site_panel' => array(
                    'title'             => esc_html__('Style Setting','zum'),
                    'description'       => esc_html__('Style Setting >','zum'),
                    'priority'          =>  101,
                ),
                'sidebar_panel' => array(
                    'title'             => esc_html__('Sidebar','zum'),
                    'description'       => esc_html__('Sidebar Setting','zum'),
                    'priority'          => 103,
                ),
                'zum_option_panel' => array(
                    'title'             => esc_html__('Option','zum'),
                    'description'       => '',
                    'priority'          => 104,
                ),
            );

            $this->customizers = array(
                'title_tagline' => array(
                    'title' => esc_html__('Site Identity', 'zum'),
                    'priority'  =>  1,
                    'settings' => array(
                        'zum_logo' => array(
                            'class' => 'image',
                            'label' => esc_html__('Logo', 'zum'),
                            'description' => esc_html__('Upload Logo Image', 'zum'),
                            'priority' => 12
                        ),
                    )
                ),
//2.General ============================================================================================================
                'zum_general' => array(
                    'title' => esc_html__('General', 'zum'),
                    'description' => '',
                    'priority' => 2,
                    'settings' => array(

                        'zum_bg_body' => array(
                            'label'         => esc_html__('Background - Body', 'zum'),
                            'description'   => '',
                            'class'         => 'color',
                            'priority'      => 2,
                            'params'        => array(
                                'default'   => '',
                            ),
                        ),
                        'zum_primary_body' => array(
                            'label'         => esc_html__('Primary - Color', 'zum'),
                            'description'   => '',
                            'class'         => 'color',
                            'priority'      => 1,
                            'params'        => array(
                                'default'   => '',
                            ),
                        ),
                        'zum_secondary_body' => array(
                            'label'         => esc_html__('Secondary - Color', 'zum'),
                            'description'   => '',
                            'class'         => 'color',
                            'priority'      => 2,
                            'params'        => array(
                                'default'   => '',
                            ),
                        ),
                    )
                ),
//3.Header =============================================================================================================
                'zum_header' => array(
                    'title' => esc_html__('Header', 'zum'),
                    'description' => '',
                    'priority' => 3,
                    'settings' => array(
                        //header
                        'zum_header_heading' => array(
                            'class' => 'heading',
                            'label' => esc_html__('Header', 'zum'),
                            'priority' => 0,
                        ),

                        'zum_header' => array(
                            'class'=> 'layout',
                            'label' => esc_html__('Header Layout', 'zum'),
                            'priority' =>1,
                            'choices' => array(
                                'simple'                   => get_template_directory_uri().'/assets/images/header/default.png',
                                'center'                   => get_template_directory_uri().'/assets/images/header/center.png',
                            ),
                            'params' => array(
                                'default' => 'simple',
                            ),
                        ),

                        'zum_bg_header' => array(
                            'label'         => esc_html__('Background - Header', 'zum'),
                            'description'   => '',
                            'class'         => 'color',
                            'priority'      => 5,
                            'params'        => array(
                                'default'   => '',
                            ),
                        ),

                        'zum_color_menu' => array(
                            'label'         => esc_html__('Color - Text', 'zum'),
                            'description'   => '',
                            'class'         => 'color',
                            'priority'      => 6,
                            'params'        => array(
                                'default'   => '',
                            ),
                        ),
                    )
                ),
//4.Footer =============================================================================================================
                'zum_new_section_footer' => array(
                    'title' => esc_html__('Footer', 'zum'),
                    'description' => '',
                    'priority' => 4,
                    'settings' => array(
                        'zum_footer' => array(
                            'type' => 'select',
                            'label' => esc_html__('Choose Footer Style', 'zum'),
                            'description' => '',
                            'priority' => -1,
                            'choices' => array(
                                '1'     => esc_html__('Footer 1', 'zum'),
                                '2'     => esc_html__('Footer 2', 'zum'),
                                'hidden' => esc_html__('Hidden Footer', 'zum')
                            ),
                            'params' => array(
                                'default' => '1',
                            ),
                        ),


                        'zum_enable_footer' => array(
                            'type' => 'checkbox',
                            'label' => esc_html__('Enable Footer', 'zum'),
                            'description' => '',
                            'priority' => 0,
                            'params' => array(
                                'default' => '1',
                            ),
                        ),
                        'zum_enable_copyright' => array(
                            'type' => 'checkbox',
                            'label' => esc_html__('Enable Copyright', 'zum'),
                            'description' => '',
                            'priority' => 0,
                            'params' => array(
                                'default' => '1',
                            ),
                        ),
                        'zum_copyright_text' => array(
                            'type' => 'textarea',
                            'label' => esc_html__('Footer Copyright Text', 'zum'),
                            'description' => '',
                            'priority' => 0,
                        ),

                        'zum_bg_footer' => array(
                            'label'         => esc_html__('Background - Footer', 'zum'),
                            'description'   => '',
                            'class'         => 'color',
                            'priority'      => 5,
                            'params'        => array(
                                'default'   => '',
                            ),

                        ),
                        'zum_color_footer' => array(
                            'label'         => esc_html__('Color - Text ', 'zum'),
                            'description'   => '',
                            'class'         => 'color',
                            'priority'      => 6,
                            'params'        => array(
                                'default'   => '',
                            ),

                        ),
                    )
                ),

//5.Categories Blog ====================================================================================================
                'zum_blog' => array(
                    'title' => esc_html__('Blogs Categories', 'zum'),
                    'description' => '',
                    'priority' => 5,
                    'settings' => array(

                        'zum_sidebar_cat' => array(
                            'class'         => 'layout',
                            'label'         => esc_html__('Sidebar Layout', 'zum'),
                            'priority'      =>3,
                            'choices'       => array(
                                'left'         => get_template_directory_uri().'/assets/images/left.png',
                                'right'        => get_template_directory_uri().'/assets/images/right.png',
                                'full'         => get_template_directory_uri().'/assets/images/full.png',
                            ),
                            'params' => array(
                                'default' => 'right',
                            ),
                        ),
                        'zum_siderbar_cat_info' => array(
                            'class' => 'info',
                            'label' => esc_html__('Info', 'zum'),
                            'description' => esc_html__( 'Please goto Appearance > Widgets > drop drag widget to the sidebar Article.', 'zum' ),
                            'priority' => 4,
                        ),
                        //post-layout-cat
                        'zum_title_cat_heading' => array(
                            'class' => 'heading',
                            'label' => esc_html__('Post Title Category', 'zum'),
                            'priority' =>5,
                        ),
                        'zum_post_title_heading' => array(
                            'class' => 'toggle',
                            'label' => esc_html__('Title Category ','zum'),
                            'priority' => 6,
                            'params' => array(
                                'default' => true,
                            ),
                        ),

                        'zum_post_cat_layout' => array(
                            'class' => 'heading',
                            'label' => esc_html__('Category layout', 'zum'),
                            'priority' =>8,
                        ),
                        'zum_layout_cat_content' => array(
                            'class'         => 'layout',
                            'priority'      =>9,
                            'choices'       => array(
//                                'tran'        => get_template_directory_uri().'/assets/images/box-tran.jpg',
                                'grid'        => get_template_directory_uri().'/assets/images/box-grid.jpg',
                                'list'        => get_template_directory_uri().'/assets/images/box-list.jpg',
                            ),
                            'params' => array(
                                'default' => 'list',
                            ),
                        ),
                        'zum_number_post_cat' => array(
                            'class' => 'slider',
                            'label' => esc_html__('Number post on a row', 'zum'),
                            'description' => '',
                            'priority' =>10,
                            'choices' => array(
                                'max' => 4,
                                'min' => 1,
                                'step' => 1
                            ),
                            'params'      => array(
                                'default' =>1
                            ),
                        ),
                        //post article content
                        'zum_post_cat_article' => array(
                            'class' => 'heading',
                            'label' => esc_html__('Post content', 'zum'),
                            'priority' =>11,
                        ),
                        'zum_post_entry_content' => array(
                            'class' => 'toggle',
                            'label' => esc_html__('Content ','zum'),
                            'priority' => 12,
                            'params' => array(
                                'default' => true,
                            ),
                        ),
                        'zum_number_content_post' => array(
                            'class' => 'slider',
                            'label' => esc_html__('Number of words in the description content', 'zum'),
                            'description' => '',
                            'priority' =>13,
                            'choices' => array(
                                'max' => 50,
                                'min' => 20,
                                'step' => 5
                            ),
                            'params'      => array(
                                'default' =>50
                            ),
                        ),

                        //post meta
                        'zum_cat_meta' => array(
                            'class' => 'heading',
                            'label' => esc_html__('Post meta', 'zum'),
                            'priority' =>13,
                        ),

                        'zum_post_meta_author' => array(
                            'class' => 'toggle',
                            'label' => esc_html__('Author ','zum'),
                            'priority' => 15,
                            'params' => array(
                                'default' => true,
                            ),
                        ),
                        'zum_post_meta_date' => array(
                            'class' => 'toggle',
                            'label' => esc_html__('Date ','zum'),
                            'priority' => 16,
                            'params' => array(
                                'default' => true,
                            ),
                        ),
                        'zum_post_meta_comment' => array(
                            'class' => 'toggle',
                            'label' => esc_html__('Comment ','zum'),
                            'priority' => 17,
                            'params' => array(
                                'default' => true,
                            ),
                        ),
                        'zum_cat_content_like' => array(
                            'class' => 'toggle',
                            'label' => esc_html__('Like ','zum'),
                            'priority' => 18,
                            'params' => array(
                                'default' => false,
                            ),
                        ),
                    ),
                ),
//6.Single blog ========================================================================================================
                'zum_blog_single' => array(
                    'title' => esc_html__('Blog Single', 'zum'),
                    'description' => '',
                    'priority' => 6,
                    'settings' => array(
                        'zum_sidebar_single' => array(
                            'class'         => 'layout',
                            'label'         => esc_html__('Sidebar Layout', 'zum'),
                            'priority'      =>13,
                            'choices'       => array(
                                'left'         => get_template_directory_uri().'/assets/images/left.png',
                                'right'        => get_template_directory_uri().'/assets/images/right.png',
                                'full'         => get_template_directory_uri().'/assets/images/full.png',
                            ),
                            'params' => array(
                                'default' => 'right',
                            ),
                        ),

                        'zum_siderbar_single_info' => array(
                            'class' => 'info',
                            'label' => esc_html__('Info', 'zum'),
                            'description' => esc_html__( 'Please goto Appearance > Widgets > drop drag widget to the sidebar Article.', 'zum' ),
                            'priority' => 14,
                        ),

                        //share
                        'zum_single_share' => array(
                            'class' => 'heading',
                            'label' => esc_html__('Share', 'zum'),
                            'priority' =>19,
                        ),
                        'zum_post_meta_share' => array(
                            'class' => 'toggle',
                            'label' => esc_html__('Share ','zum'),
                            'priority' => 20,
                            'params' => array(
                                'default' => false,
                            ),
                        ),
                        'zum_share_facebook' => array(
                            'class' => 'toggle',
                            'label' => esc_html__('Share Facebook  ','zum'),
                            'priority' => 21,
                            'params' => array(
                                'default' => true,
                            ),
                        ),
                        'zum_share_twitter' => array(
                            'class' => 'toggle',
                            'label' => esc_html__('Share Twitter  ','zum'),
                            'priority' => 22,
                            'params' => array(
                                'default' => true,
                            ),
                        ),
                        'zum_share_google' => array(
                            'class' => 'toggle',
                            'label' => esc_html__('Share Google  ','zum'),
                            'priority' => 23,
                            'params' => array(
                                'default' => true,
                            ),
                        ),

                        'zum_share_linkedin' => array(
                            'class' => 'toggle',
                            'label' => esc_html__('Share Linkedin  ','zum'),
                            'priority' => 24,
                            'params' => array(
                                'default' => false,
                            ),
                        ),

                        'zum_share_pinterest' => array(
                            'class' => 'toggle',
                            'label' => esc_html__('Share Pinterest  ','zum'),
                            'priority' => 25,
                            'params' => array(
                                'default' => false,
                            ),
                        ),
                        //comments
                        'zum_single_comments' => array(
                            'class' => 'heading',
                            'label' => esc_html__('Comments', 'zum'),
                            'priority' =>28,
                        ),
                        'zum_comments_single_facebook' => array(
                            'class' => 'toggle',
                            'label' => esc_html__('Enable Facebook Comments ','zum'),
                            'priority' => 29,
                            'params' => array(
                                'default' => false,
                            ),
                        ),
                        'zum_comments_single' => array(
                            'type'          => 'text',
                            'label'         => esc_html__('Your app id :', 'zum'),
                            'priority'      => 30,
                            'params'        => array(
                                'default'   => '',
                            ),
                        ),
                        'zum_comments_single_info' => array(
                            'class' => 'info',
                            'label' => esc_html__('Info', 'zum'),
                            'description' => esc_html__('If you want show notification on  your facebook , please input app id ...', 'zum' ),
                            'priority' => 31,
                        ),
                    ),
                ),
//7.Adsense blog ========================================================================================================
                'zum_ads' => array(
                    'title' => esc_html__('Adsense Setting', 'zum'),
                    'description' => '',
                    'priority' => 7,
                    'settings' => array(

                        'zum_ads_rectangle' => array(
                            'type' => 'textarea',
                            'label' => esc_html__(' ADS Size: Large Rectangle', 'zum'),
                            'description' => '',
                            'priority' => 1,
                        ),
                        'zum_ads_rectangle_info' => array(
                            'class' => 'info',
                            'label' => esc_html__('Info', 'zum'),
                            'description' => esc_html__('Add code adsbygoogle with the size is: 250x360,336x280 ,300x250 ...', 'zum' ),
                            'priority' => 2,
                        ),
                        'zum_ads_leaderboard' => array(
                            'type' => 'textarea',
                            'label' => esc_html__('ADS Size: Leaderboard', 'zum'),
                            'description' => 'Add code adsbygoogle with the size is: 468x60 ,728x90, 920x180 ...',
                            'priority' => 3,
                        ),
                        'zum_heading_ads_single' => array(
                            'class' => 'heading',
                            'label' => esc_html__('Single', 'zum'),
                            'priority' =>20,
                        ),
                        'zum_ads_single_article' => array(
                            'class' => 'toggle',
                            'label' => esc_html__('Ads at the end of the article ','zum'),
                            'priority' => 21,
                            'params' => array(
                                'default' => false,
                            ),
                        ),
                        'zum_ads_single_comment' => array(
                            'class' => 'toggle',
                            'label' => esc_html__('Ads at the top of the Comment ','zum'),
                            'priority' => 21,
                            'params' => array(
                                'default' => false,
                            ),
                        ),
                    )
                ),
//Font   ===============================================================================================================
                'zum_new_section_font_size' => array(
                    'title' => esc_html__('Font', 'zum'),
                    'priority' => 8,
                    'settings' => array(
                        'zum_body_font_google' => array(
                            'type'          => 'select',
                            'label'         => esc_html__('Use Google Font', 'zum'),
                            'choices'       => zum_googlefont(),
                            'priority'      => 0,
                            'params'        => array(
                                'default'       => 'Poppins',
                            ),

                        ),
                        'zum_body_font_size' => array(
                            'class' => 'slider',
                            'label' => esc_html__('Font size ', 'zum'),
                            'description' => '',
                            'priority' =>8,
                            'choices' => array(
                                'max' => 30,
                                'min' => 10,
                                'step' => 1
                            ),
                            'params'      => array(
                                'default' => 14,
                            ),
                        ),
                        'zum_title_font_google' => array(
                            'type'          => 'select',
                            'label'         => esc_html__('Title Font', 'zum'),
                            'choices'       => zum_googlefont(),
                            'priority'      => 15,
                            'params'        => array(
                                'default'       => 'Noto Serif',
                            ),

                        ),
                        'zum_title_font_size' => array(
                            'class' => 'slider',
                            'label' => esc_html__('Title Font size ', 'zum'),
                            'description' => '',
                            'priority' =>16,
                            'choices' => array(
                                'max' => 30,
                                'min' => 10,
                                'step' => 1
                            ),
                            'params'      => array(
                                'default' => 18,
                            ),
                        ),
                    )
                ),
//Style  ===============================================================================================================


            );
        }

        public function zum_customizer_script()
        {
            // Register
            wp_enqueue_style('na-customize', get_template_directory_uri() . '/inc/customize/assets/css/customizer.css', array(),null);
            wp_enqueue_style('jquery-ui', get_template_directory_uri() . '/inc/customize/assets/css/jquery-ui.min.css', array(),null);
            wp_enqueue_script('na-customize', get_template_directory_uri() . '/inc/customize/assets/js/customizer.js', array('jquery'), null, true);
        }

        public function add_customize($customizers) {
            $this->customizers = array_merge($this->customizers, $customizers);
        }


        public function zum_register_theme_customizer()
        {
            global $wp_customize;

            foreach ($this->customizers as $section => $section_params) {

                //add section
                $wp_customize->add_section($section, $section_params);
                if (isset($section_params['settings']) && count($section_params['settings']) > 0) {
                    foreach ($section_params['settings'] as $setting => $params) {

                        //add setting
                        $setting_params = array();
                        if (isset($params['params'])) {
                            $setting_params = $params['params'];
                            unset($params['params']);
                        }
                        $wp_customize->add_setting($setting, array_merge( array( 'sanitize_callback' => null ), $setting_params));
                        //Get class control
                        $class = 'WP_Customize_Control';
                        if (isset($params['class']) && !empty($params['class'])) {
                            $class = 'WP_Customize_' . ucfirst($params['class']) . '_Control';
                            unset($params['class']);
                        }

                        //add params section and settings
                        $params['section'] = $section;
                        $params['settings'] = $setting;

                        //add controll
                        $wp_customize->add_control(
                            new $class($wp_customize, $setting, $params)
                        );
                    }
                }
            }

            foreach($this->panels as $key => $panel){
                $wp_customize->add_panel($key, $panel);
            }

            return;
        }

        public function remove_default_customize_section()
        {
            global $wp_customize;
//            // Remove Sections
//            $wp_customize->remove_section('title_tagline');
            $wp_customize->remove_section('header_image');
            $wp_customize->remove_section('nav');
            $wp_customize->remove_section('static_front_page');
            $wp_customize->remove_section('colors');
            $wp_customize->remove_section('background_image');
        }
    }
}