<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/pt-br:Editando_wp-config.php
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações
// com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define('DB_NAME', 'wp_lulu');

/** Usuário do banco de dados MySQL */
define('DB_USER', 'wp_lulu');

/** Senha do banco de dados MySQL */
define('DB_PASSWORD', 'md4Z48d%');

/** Nome do host do MySQL */
define('DB_HOST', 'localhost');

/** Charset do banco de dados a ser usado na criação das tabelas. */
define('DB_CHARSET', 'utf8mb4');

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Ne;hc5Wqy1k.RWGfZV5)h~mxLHV]II|@I}a.=hcnxvlCN:kGi9Gc0nj^1B60_Fb1');
define('SECURE_AUTH_KEY',  '[ s9^5%Xkf9~g&U;ISQXxonu$8o~Vv}s-($zcP_.ackw@.hwGmWL,Mr#]SJt~6PM');
define('LOGGED_IN_KEY',    '])+^S<D/boBc|Nw/O9d2)gcHYk/tB^rE{BiaoDyu|z;xg*/ch_CCKYu[2yV5wU]p');
define('NONCE_KEY',        'P;c@Q!+c]*cn7z>V@3zx4R/6e/ftsA3[9c6{u}<aRVl4w)@TV/L?fMPDU{t4jqG-');
define('AUTH_SALT',        'I.?RAl3v?cAMfr|WUG$cv$=4eyu$t/cMvUcYf_%Ge-a:A;@}]E*U$3:,jv&j!1H@');
define('SECURE_AUTH_SALT', 'HdECi. {V%%h1|KgdP5j~$_ERma*,Mz8+J+DIeDT}+Ai!m1MZ7((t:Ru(oK<R;Ju');
define('LOGGED_IN_SALT',   ':ni-jR||b-yFh]yD]> VlZ(~<R<-I2<;@m[}s8,bc-jS)3B[/3VL)XwT&^LA-!=6');
define('NONCE_SALT',       'Y;Rd,{J4F!G{h-V2e?DsiU!L4yE[~}Ed=V1#O&f-*8VP<[dDp1so,,BKO*sc;Pku');

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix  = 'wp_';

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://codex.wordpress.org/pt-br:Depura%C3%A7%C3%A3o_no_WordPress
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Configura as variáveis e arquivos do WordPress. */
require_once(ABSPATH . 'wp-settings.php');
